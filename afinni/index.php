<?php
/**
 * Created by PhpStorm.
 * User: the1jjf1
 * Date: 23.10.17
 * Time: 11:04
 */
ini_set("display_errors", "On");
include_once __DIR__ . "/Affine.php";

$affine = new Affine((isset($_GET['a']) ? $_GET['a'] : null), (isset($_GET['b']) ? $_GET['b'] : null));

/*
$text = "Ahoj pepo jak se máš";
echo $text . "<br><br>";
$encrepted = ($affine->encrypt($text));
echo $encrepted . "<br><br>";
echo($affine->decrypt($encrepted));
*/
?>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="default.css">
    <title>Afinní šifra</title>
</head>
<body>
<form>
    <h1>Encrypt</h1>
    <label>
        <span>Parametr a:</span>
        <select id="a" name="a">
            <option value="1">1</option>
            <option value="3">3</option>
            <option value="7">7</option>
            <option value="17" selected>17</option>
        </select>
    </label><br>
    <label>
        <span>Parametr b:</span>
        <select id="b" name="b">
            <?php
            for ($i = 0; $i < 26; $i++) {
                if ($i == 5) {
                    echo "<option value='$i' selected>$i</option>";
                } else {
                    echo "<option value='$i'>$i</option>";
                }
            }
            ?>
        </select>
    </label><br>
    <textarea placeholder="Nezašifrovaný text" name="to_encrypt"></textarea><br>
    <textarea placeholder="Zašifrovaný text" disabled><?php
        echo $affine->encrypt((isset($_GET['to_encrypt']) ? $_GET['to_encrypt'] : null));
        ?></textarea>
    <input type="submit" value="Enryptovat">
</form>
<form>
    <h1>Decrypt</h1>
    <label>
        <span>Parametr a:</span>
        <select id="a" name="a">
            <option value="1">1</option>
            <option value="3">3</option>
            <option value="7">7</option>
            <option value="17" selected>17</option>
        </select>
    </label><br>
    <label>
        <span>Parametr b:</span>
        <select id="b" name="b">
            <?php
            for ($i = 0; $i < 26; $i++) {
                if ($i == 5) {
                    echo "<option value='$i' selected>$i</option>";
                } else {
                    echo "<option value='$i'>$i</option>";
                }
            }
            ?>
        </select>
    </label><br>
    <textarea placeholder="Zašifrovaný text" name="to_decrypt"></textarea><br>
    <textarea placeholder="Nezašifrovaný text" disabled><?php
        echo $affine->decrypt((isset($_GET['to_decrypt']) ? $_GET['to_decrypt'] : null));
        ?></textarea>
    <input type="submit" value="Decryptovat">
</form>
</body>
</html>
