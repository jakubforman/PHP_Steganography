$(function () {
    $("#form2").on("submit", function (e) {
        e.preventDefault();
        $("#submitDecrypt").prop("disabled", true);
        var fDecrypteSRC = $("#fileToDecrypt").val();
        var bits = $("#bitLengths").val();
        var $textDecrypted = $("#textDecrypted");
        $.post("fcs.php?decrypt=true&src="+fDecrypteSRC+"&bits=" +bits, {}, function (clbck) {
            $textDecrypted.val(clbck);
            $("#submitDecrypt").prop("disabled", false);
        });
    });
    $("#fileToDecrypt").on("change keyup paste", function () {
        if ($(this).val() !== "" && $("#bitLengths").val() !== "") {
            $("#submitDecrypt").prop("disabled", false);
        } else {
            $("#submitDecrypt").prop("disabled", true);
        }
    });
    $("#bitLengths").on("change keyup paste", function () {
        if ($(this).val() !== "" && $("#fileToDecrypt").val() !== "") {
            $("#submitDecrypt").prop("disabled", false);
        } else {
            $("#submitDecrypt").prop("disabled", true);
        }
    });
});