<?php
/**
 * Created by PhpStorm.
 * User: the1jjf1
 * Date: 06.10.17
 * Time: 10:58
 */
if (isset($_GET['encrypt'])) {
    if (isset($_GET['src'])) {
        $src = $_GET['src'];
    } else {
        $src = null;
    }
    if (isset($_GET['msg'])) {
        $msg = $_GET['msg'];
    } else {
        $msg = null;
    }

    include_once __DIR__ . "/Steganography.php";
    $steganography = new Steganography($src);

    echo json_encode($steganography->Encode($msg));
}
if (isset($_GET['decrypt'])) {
    if (isset($_GET['src'])) {
        $src = $_GET['src'];
    } else {
        $src = null;
    }
    include_once __DIR__ . "/Steganography.php";
    $steganography = new Steganography($src);
    echo $steganography->Decode($_GET['bits']);
}
if (isset($_GET['files'])) {
    $data = array();

    $error = false;
    $files = array();

    $uploaddir = 'uploads/';
    foreach ($_FILES as $file) {
        if (move_uploaded_file($file['tmp_name'], $uploaddir . basename($file['name']))) {
            $files[] = $uploaddir . $file['name'];
        } else {
            $error = true;
        }
    }
    $data = ($error) ? array('error' => 'There was an error uploading your files') : array('files' => $files);
    echo json_encode($data);
}


